### This script was written to support antibiotic resistance research for the C. diff genomes.
### It is a guide on how to extract the fasta files for the genes of interest, for downstream alignment.
### The list of resistance genes is provided by CARD ###

# Pull out the genomes that have the resistance gene of interest. Create a two column list that can be used as a reference for extracting the sequences

for i in *
 do
  grep [Gene_Name] "${i}" > "${i}_[Gene_Name].txt"
 done
 
for i in *[Gene_Name].txt
 do
  cat "${i}" >> [Gene_Name]_list.txt
  rm $i
 done

awk ' {print $1, $2}' [Gene_Name]_list.txt > [Gene_Name]_list.awk

# Extract that sequences for the genes of interest using the two column reference list  

module load samtools

while read i j
 do
  cd /sc/orga/work/chackk02/C_diff/Antibiotic_Resistance/Final/CARD-Analysis/$i
   samtools faidx "${i}.faa" "${j}" > "${i}_${j}"
   sed 's/^\(>.*\)$/\>'"${i}_${j}"'/' "${i}_${j}" > "${i}_${j}.fasta"
   cd /sc/orga/work/chackk02/C_diff/Antibiotic_Resistance/Final/Gene-Analysis
  done < [Gene_Name]_list.awk