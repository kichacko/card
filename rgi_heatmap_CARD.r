#!/usr/utilities/card/

# Load required libraries
library("reshape2")
library("gplots")

# Load datasets
Combined_CARD <- read.delim("~/Google Drive/1 - Lab/2 - C Diff/5 - Antibiotic Resistance/3 - Data/combined_CARD_full.txt")
MLST_categories <- read.delim("~/Google Drive/1 - Lab/2 - C Diff/5 - Antibiotic Resistance/3 - Data/MLST_categories_full.txt")
GENE_renaming <- read.delim2("~/Google Drive/1 - Lab/2 - C Diff/5 - Antibiotic Resistance/3 - Data/GENE_renaming.txt")
PATIENT_ID <- read.delim("~/Google Drive/1 - Lab/2 - C Diff/5 - Antibiotic Resistance/3 - Data/PATIENT_ID.txt")

# Clean up datasets
MLST_categories$MLST[MLST_categories$MLST == ""] <- "0"

# Combine MLST and GENE into one dataframe
CARD_full <- Combined_CARD
CARD_full <- merge(CARD_full, MLST_categories, all.x = TRUE)
CARD_full <- merge(CARD_full, GENE_renaming, all.x = TRUE)
CARD_full <- merge(CARD_full, PATIENT_ID, all.x =  TRUE)

# Add a HEATMAP column that feeds a "1" for every strict and perfect hit
CARD_full$HEATMAP <- ifelse(CARD_full$CUT_OFF == "Strict", 1, ifelse(CARD_full$CUT_OFF == "Perfect", 1, 0))
CARD_full$ISOLATE_ID <- paste(CARD_full$ISOLATE, CARD_full$PT_ID, sep="--")

# We only want to analyze the Strict and Perfect hits
CARD_perfect_strict <- subset(CARD_full, CARD_full$CUT_OFF == "Perfect" | CARD_full$CUT_OFF == "Strict")

# Subsetting data in order to feed into acast
CARD_dataframe <- subset(CARD_perfect_strict, select = c("ISOLATE_ID", "MLST", "GENE", "CLASS", "HEATMAP"))

#Set up matrix for graphing. Use dcast because it ensures that all data is ordered by MLST type. That data can then feed into the MLST_types data.frame to serve RowSideColors.
CARD_matrix <- dcast(CARD_dataframe, CARD_dataframe$MLST + CARD_dataframe$ISOLATE_ID ~ CARD_dataframe$GENE, value.var="HEATMAP", fun.aggregate = length)
names(CARD_matrix)[1] <- "MLST"
names(CARD_matrix)[2] <- "ISOLATE_ID"
rownames(CARD_matrix) <- CARD_matrix$ISOLATE_ID

# Subsetting data in order to feed into RowSideColors
MLST_types <- subset(CARD_matrix, select = c("MLST"))
MLST_types_only <- unique(MLST_types)
MLST_types_only$COLOR <- c("#7F7CA9",
							"#2ED8FF",
							"#4A7BE1",
							"#6EC4E1",
							"#9292EE",
							"#33819E",
							"#7899ED",
							"#56748E",
							"#63A5F3",
							"#62719C",
							"#50B3E3",
							"#6272BE",
							"#549FBD",
							"#AFB4F3",
							"#4685BC",
							"#B3ADDC",
							"#3D8AD7",
							"#81BBE3",
							"#919AD9",
							"#3AA6E9",
							"#8D9DC9",
							"#88ADE6")
row_side_color <- merge(MLST_types, MLST_types_only)
	
# Subsetting data in order to feed into ColSideColors. DCAST doesn't support separation by rows, so we need to get the data from dataframe and order appropriately 
DRUG_types <- subset(CARD_dataframe, select = c("GENE", "CLASS"))
DRUG_types <- unique(DRUG_types)
DRUG_types_only <- unique(subset(DRUG_types, select = c("CLASS")))
DRUG_types_only$COLOR <- c("#4AAC6F",
							"#61E33A",
							"#657957",
							"#9AD73E",
							"#467B46",
							"#65E273",
							"#8EAE75",
							"#4EA535",
							"#C1E7A8",
							"#5B852B",
							"#77E39C")
col_side_color <- merge(DRUG_types, DRUG_types_only)
col_side_color <- col_side_color[order(col_side_color$GENE), ]

# Now that we have extracted the data we need from the CARD_matrix dataframe, clean it up so its ready to be imported into the heatmap
CARD_matrix <- CARD_matrix[, -(1:2)]
CARD_matrix <- as.matrix(CARD_matrix)

# Run heatmap using gplots heatmap.2
plot(c(0, 100), c(0, 100), type = "n", xlab = "", ylab = "")

CARD_heatmap <- heatmap.2(CARD_matrix, 
	col = rev(gray.colors(256, start = 0, end = 1)), 
	RowSideColors = row_side_color$COLOR, 
	ColSideColors = col_side_color$COLOR, 
	margin = c(18, 15), 
	tracecol = NA, 
	key = FALSE,
	lwid = c(1.5,4), 
	lhei = c(1.5,4)
)

text(55, 0, "ABX Gene", cex = 0.75)
text(100, 50, "Isolate ID",cex = 0.75, srt = 90)

# Construct Legends and Text
legend_scale <- as.numeric(1)

text(0, 100, "MLST Type", col = "#7F7CA9", cex = 0.75, adj = c(0,1))
row_legend <- unique(subset(row_side_color, select = c("MLST", "COLOR")))
for (i in nrow(row_legend):1)
	{
	j = i * legend_scale
	rect(0, 99-j, 3, 99-j-legend_scale, border = NA, col = row_legend[i, 2])
	text(4, 99-j, row_legend[i, 1], col = row_legend[i, 2], cex = 0.50, adj = c(0,1))
	}

text(8, 100, "ABX Class", col = "#4AAC6F", cex = 0.75, adj = c(0,1))
col_legend <- unique(subset(col_side_color, select = c("CLASS", "COLOR")))
for (i in nrow(col_legend):1)
{
	j = i * legend_scale
	rect(8, 99-j, 11, 99-j-legend_scale, border = NA, col = col_legend[i, 2])
	text(12, 99-j, col_legend[i, 1], col = col_legend[i, 2], cex = 0.50, adj = c(0,1))
	}