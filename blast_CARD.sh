#! usr/bin/sh

# Build databases

makeblastdb -in reference_protein_homolog_model.fasta -dbtype prot
makeblastdb -in reference_protein_variant_model.fasta -dbtype prot
makeblastdb -in reference_protein_wildtype_model.fasta -dbtype prot

for i in reference*.fasta.*
do
    mv $i database_$i
done

cd ..

# Run blastp for each isolate

for i in CD*
do
    cd $i
        blastp -db ../reference/database_reference_protein_homolog_model.fasta -query "${i}.faa" -outfmt 6  -max_target_seqs 1 -evalue 1e-30 -out "${i}_homolog.out"
        blastp -db ../reference/database_reference_protein_variant_model.fasta -query "${i}.faa" -outfmt 6 -max_target_seqs 1 -evalue 1e-30 -out "${i}_variant.out"
        blastp -db ../reference/database_reference_protein_wildtype_model.fasta -query "${i}.faa" -outfmt 6 -max_target_seqs 1 -evalue 1e-30 -out "${i}_wildtype.out"
    cd ..
done

for i in CD*
do
    cd $i
        tr '|' $'\t' < "${i}_homolog.out" > "${i}_homolog.tr"
        awk '!x[$5]++' "${i}_homolog.tr" > "${i}_homolog.awk"    
        awk -v var="${i}" ' BEGIN{OFS=FS="\t"}
            {if (NR != 1)
    	        {print var, $0}
            }' "${i}_homolog.awk" > "${i}_homolog_2.awk"
        awk 'BEGIN{OFS=FS="\t"}
            {print $1, $2, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16}' "${i}_homolog_2.awk" > "${i}_CARD_clean.txt"
        cp "${i}_CARD_clean.txt" ../
        rm *.tr *.awk
    cd ..
done

# Combine all the CARD files together and add header line
for i in *clean.txt
do
    cat $i >> Combined_CARD_clean.txt
    rm $i
done

awk 'BEGIN{print "ISOLATE_ID" "\t" "PROKKA_ID" "\t" "ARO" "\t" "GENE" "\t" "Percentage-of-Identical-Matches" "\t" "Alignment-Length" "\t" "Number-of-Mismatches" "\t" "#-Gap-Ppenings" "\t" "Start-Query" "\t" "End-Query" "\t" "Ref-Start" "\t" "Ref-End" "\t" "E-Value" "\t" "Bitscore"}1' Combined_CARD_clean.txt > Combined_CARD.txt
rm Combined_CARD_clean.txt





