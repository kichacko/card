library("reshape2")
library("gplots")

Vancomycin_Mapping <- read.delim("~/Google Drive/1 - Lab/2 - C Diff/5 - Antibiotic Resistance/1 - CARD/2 - Blast-Based/Vancomycin_Mapping.txt")

Vancomycin_Mapping <- subset(Vancomycin_Mapping, Vancomycin_Mapping$E.Value < 1e-30)
Vancomycin_Mapping <- subset(Vancomycin_Mapping, select = c("Isolate_ID", "ARO", "Gene", "Description"))

CARD_matrix <- dcast(Vancomycin_Mapping, Vancomycin_Mapping$Isolate_ID ~ Vancomycin_Mapping$Gene, value.var = "Gene", fun.aggregate = length)
names(CARD_matrix)[1] <- "Isolate_ID"

VanA_matrix <- subset(CARD_matrix, select = c("Isolate_ID", "vanA", "vanHA", "vanRA", "vanSA", "vanXA", "vanYA"))
VanA_matrix <- VanA_matrix[!VanA_matrix$Isolate_ID %in% c("VanB Operon", "VanC Operon", "VanD Operon", "VanE Operon", "VanG Operon"), ]
names(VanA_matrix)[1] <- "Isolate_ID"
rownames(VanA_matrix) <- VanA_matrix$Isolate_ID
VanA_matrix <- VanA_matrix[, -1]
VanA_matrix[VanA_matrix == 0] <- "#FFFFFF"
VanA_matrix[VanA_matrix == 1] <- "#8772c9"

VanB_matrix <- subset(CARD_matrix, select = c("Isolate_ID", "vanB", "vanHB", "vanRB", "vanSB", "vanWB", "vanXB", "vanYB"))
VanB_matrix <- VanB_matrix[!VanB_matrix$Isolate_ID %in% c("VanA Operon", "VanC Operon", "VanD Operon", "VanE Operon", "VanG Operon"), ]
names(VanB_matrix)[1] <- "Isolate_ID"
rownames(VanB_matrix) <- VanB_matrix$Isolate_ID
VanB_matrix <- VanB_matrix[, -1]
VanB_matrix[VanB_matrix == 0] <- "#FFFFFF"
VanB_matrix[VanB_matrix == 1] <- "#70a845"

VanC_matrix <- subset(CARD_matrix, select = c("Isolate_ID", "vanC", "vanRC", "vanSC", "vanTC", "vanXYC"))
VanC_matrix <- VanC_matrix[!VanC_matrix$Isolate_ID %in% c("VanA Operon", "VanB Operon", "VanD Operon", "VanE Operon", "VanG Operon"), ]
names(VanC_matrix)[1] <- "Isolate_ID"
rownames(VanC_matrix) <- VanC_matrix$Isolate_ID
VanC_matrix <- VanC_matrix[, -1]
VanC_matrix[VanC_matrix == 0] <- "#FFFFFF"
VanC_matrix[VanC_matrix == 1] <- "#c8598f"

VanD_matrix <- subset(CARD_matrix, select = c("Isolate_ID", "vanD", "vanHD", "vanRD", "vanSD", "vanXD", "vanYD"))
VanD_matrix <- VanD_matrix[!VanD_matrix$Isolate_ID %in% c("VanA Operon", "VanB Operon", "VanC Operon", "VanE Operon", "VanG Operon"), ]
names(VanD_matrix)[1] <- "Isolate_ID"
rownames(VanD_matrix) <- VanD_matrix$Isolate_ID
VanD_matrix <- VanD_matrix[, -1]
VanD_matrix[VanD_matrix == 0] <- "#FFFFFF"
VanD_matrix[VanD_matrix == 1] <- "#4dad97"

VanE_matrix <- subset(CARD_matrix, select = c("Isolate_ID", "vanE", "vanRE", "vanSE", "vanTE", "vanXYE"))
VanE_matrix <- VanE_matrix[!VanE_matrix$Isolate_ID %in% c("VanA Operon", "VanB Operon", "VanC Operon", "VanD Operon", "VanG Operon"), ]
names(VanE_matrix)[1] <- "Isolate_ID"
rownames(VanE_matrix) <- VanE_matrix$Isolate_ID
VanE_matrix <- VanE_matrix[, -1]
VanE_matrix[VanE_matrix == 0] <- "#FFFFFF"
VanE_matrix[VanE_matrix == 1] <- "#cc5a43"

VanG_matrix <- subset(CARD_matrix, select = c("Isolate_ID", "vanG", "vanRG", "vanSG", "vanTG", "vanU", "vanWG", "vanXYG", "vanYG1"))
VanG_matrix <- VanG_matrix[!VanG_matrix$Isolate_ID %in% c("VanA Operon", "VanB Operon", "VanC Operon", "VanD Operon", "VanE Operon"), ]
names(VanG_matrix)[1] <- "Isolate_ID"
rownames(VanG_matrix) <- VanG_matrix$Isolate_ID
VanG_matrix <- VanG_matrix[, -1]
VanG_matrix[VanG_matrix == 0] <- "#FFFFFF"
VanG_matrix[VanG_matrix == 1] <- "#b69040"

plot(c(0, 65), c(0, 70), type = "n", xlab = "", ylab = "", bty = "n", axes = FALSE)

for (i in 1:nrow(VanA_matrix)){
    for (j in 1:ncol(VanA_matrix)){
        rect(5+j, 0+i, 5+j+1, 0+i+1, col = VanA_matrix[i, j], border = "#D3D3D3")
        text(1, 0+i+1, rownames(VanA_matrix[i, ]), col = "#696969",cex = 0.5)
        text(5+j+0.5, 0+nrow(VanA_matrix)+3, colnames(VanA_matrix[j]), srt = 90, cex = 0.5, adj = c(0, 0.5), col = "#8772c9")
        }
    }
    
for (i in 1:nrow(VanB_matrix)){
    for (j in 1:ncol(VanB_matrix)){
        rect(15+j, 0+i, 15+j+1, 0+i+1, col = VanB_matrix[i, j], border = "#D3D3D3")
        text(15+j+0.5, 0+nrow(VanB_matrix)+3, colnames(VanB_matrix[j]), srt = 90, cex = 0.5, adj = c(0, 0.5), col = "#70a845")
        }
    }
    
for (i in 1:nrow(VanC_matrix)){
    for (j in 1:ncol(VanC_matrix)){
        rect(25+j, 0+i, 25+j+1, 0+i+1, col = VanC_matrix[i, j], border = "#D3D3D3")
        text(25+j+0.5, 0+nrow(VanC_matrix)+3, colnames(VanC_matrix[j]), srt = 90, cex = 0.5, adj = c(0, 0.5), col = "#c8598f")
        }
    }
    
for (i in 1:nrow(VanD_matrix)){
    for (j in 1:ncol(VanD_matrix)){
        rect(35+j, 0+i, 35+j+1, 0+i+1, col = VanD_matrix[i, j], border = "#D3D3D3")
        text(35+j+0.5, 0+nrow(VanD_matrix)+3, colnames(VanD_matrix[j]), srt = 90, cex = 0.5, adj = c(0, 0.5), col = "#4dad97")
        }
    }

for (i in 1:nrow(VanE_matrix)){
    for (j in 1:ncol(VanE_matrix)){
        rect(45+j, 0+i, 45+j+1, 0+i+1, col = VanE_matrix[i, j], border = "#D3D3D3")
        text(45+j+0.5, 0+nrow(VanE_matrix)+3, colnames(VanE_matrix[j]), srt = 90, cex = 0.5, adj = c(0, 0.5), col = "#cc5a43")
        }
    }
    
for (i in 1:nrow(VanG_matrix)){
    for (j in 1:ncol(VanG_matrix)){
        rect(55+j, 0+i, 55+j+1, 0+i+1, col = VanG_matrix[i, j], border = "#D3D3D3")
        text(55+j+0.5, 0+nrow(VanG_matrix)+3, colnames(VanG_matrix[j]), srt = 90, cex = 0.5, adj = c(0, 0.5), col = "#b69040")
        }
    }