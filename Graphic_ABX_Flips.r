library("reshape2")
library("gplots")


`2016.10.17...Full.Data` <- read.csv("~/Google Drive/1 - Lab/8 - Thesis/3 - Code/Identifying Resistance/2 - Full Search/2016-10-17 - Full Data.csv", sep=";")
full_data <- `2016.10.17...Full.Data`
full_data[1] <- NULL

subset_data <- subset(full_data, select = c("Isolate.ID", "Patient.ID", "Collection.Date", "Pathogen", "Agent", "MIC" ))

Enterococcus_faecium <- subset(subset_data, subset_data$Pathogen == "Enterococcus faecium" & (subset_data$Agent == "Vancomycin" | subset_data$Agent == "Linezolid")) 
Enterococcus_faecium <- Enterococcus_faecium[order(Enterococcus_faecium$Collection.Date) , ]

ID <- unique(subset(Enterococcus_faecium, select = c("Patient.ID")))
ID$ID_number <- seq.int(nrow(ID))
Enterococcus_faecium <- merge(Enterococcus_faecium, ID, all.x = TRUE)

Enterococcus_faecium$MIC_clean <- Enterococcus_faecium$MIC
Enterococcus_faecium$MIC_clean <- gsub(">", "", Enterococcus_faecium$MIC_clean)
Enterococcus_faecium$MIC_clean <- gsub("=", "", Enterococcus_faecium$MIC_clean)
Enterococcus_faecium$MIC_clean <- gsub("<", "", Enterococcus_faecium$MIC_clean)
Enterococcus_faecium$MIC_clean <- as.numeric(Enterococcus_faecium$MIC_clean)
Enterococcus_faecium$MIC_clean[is.na(Enterococcus_faecium$MIC_clean)] <- 0

Enterococcus_faecium$COLOR <- 0
Enterococcus_faecium$COLOR[Enterococcus_faecium$MIC_clean == 0] <- "#69a75b"
Enterococcus_faecium$COLOR[Enterococcus_faecium$Agent == "Linezolid" & Enterococcus_faecium$MIC_clean <= 2] <- "#69a75b"
Enterococcus_faecium$COLOR[Enterococcus_faecium$Agent == "Vancomycin" & Enterococcus_faecium$MIC_clean <= 4] <- "#6885d0"
Enterococcus_faecium$COLOR[Enterococcus_faecium$Agent == "Linezolid" & Enterococcus_faecium$MIC_clean > 2] <- "#cc5658"
Enterococcus_faecium$COLOR[Enterococcus_faecium$Agent == "Vancomycin" & Enterococcus_faecium$MIC_clean > 4] <- "#b75fb3"

matrix_Enterococcus_faecium <- dcast(Enterococcus_faecium, Enterococcus_faecium$Patient.ID + Enterococcus_faecium$Isolate.ID ~ Enterococcus_faecium$ID + Enterococcus_faecium$Agent, value.var="COLOR", fun.aggregate = NULL, fill = "#FFFFFF")


names(matrix_Enterococcus_faecium)[1] <- "PATIENT_ID"
names(matrix_Enterococcus_faecium)[2] <- "ISOLATE_ID"
y_labels <- subset(matrix_Enterococcus_faecium, select = c("ISOLATE_ID"))

rownames(matrix_Enterococcus_faecium) <- matrix_Enterococcus_faecium$ISOLATE_ID
matrix_Enterococcus_faecium <- matrix_Enterococcus_faecium[, -(1:2)]
x_labels <- as.data.frame(colnames(matrix_Enterococcus_faecium))

plot(c(0, 300), c(0, 300), type = "n", xlab = "", ylab = "", bty = "n", axes = FALSE)
for (i in 1:nrow(matrix_Enterococcus_faecium)){
  for (j in 1:ncol(matrix_Enterococcus_faecium)){
    rect(5+j, 10+i, 5+j+1, 10+i+1, col = matrix_Enterococcus_faecium[i, j], border = "#D3D3D3", lwd = 0.5)
}}

for (i in 1:nrow(y_labels)){
  text(1, 10+i, labels = y_labels[i, 1], cex = 0.2)
}

for (j in 1:nrow(x_labels)){
  text(5.5+(j), 7, labels = x_labels[j, 1], cex = 0.2, srt = 90, adj = c(1, 0.5))
}