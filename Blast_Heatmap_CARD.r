#!/usr/utilities/card/

# Load required libraries
library("reshape2")
library("gplots")

# Load datasets
Combined_CARD <- read.delim([PATH])
MLST_categories <- read.delim([PATH])

# Combine MLST and GENE into one dataframe
CARD_full <- Combined_CARD
CARD_full <- merge(CARD_full, MLST_categories, all.x = TRUE)
CARD_full <- subset(CARD_full, CARD_full$E.Value < 1e-30)
CARD_full$Heatmap <- as.numeric(1)

# Subsetting data in order to feed into acast
CARD_dataframe <- subset(CARD_full, select = c("ISOLATE_ID", "MLST", "GENE", "Heatmap"))

#Set up matrix for graphing. Use dcast because it ensures that all data is ordered by MLST type. That data can then feed into the MLST_types data.frame to serve RowSideColors.
CARD_matrix <- dcast(CARD_dataframe, CARD_dataframe$MLST + CARD_dataframe$ISOLATE_ID ~ CARD_dataframe$GENE, value.var="Heatmap", fun.aggregate = length)
names(CARD_matrix)[1] <- "MLST"
names(CARD_matrix)[2] <- "ISOLATE_ID"
rownames(CARD_matrix) <- CARD_matrix$Isolate_ID

### This needs to be fixed
# Subsetting data in order to feed into RowSideColors
MLST_types <- subset(CARD_matrix, select = c("MLST"))
MLST_types_only <- unique(MLST_types)
MLST_types_only$COLOR <- c("#7F7CA9",
							"#2ED8FF",
							"#4A7BE1",
							"#6EC4E1",
							"#9292EE",
							"#33819E",
							"#7899ED",
							"#56748E",
							"#63A5F3",
							"#62719C",
							"#50B3E3",
							"#6272BE",
							"#549FBD",
							"#AFB4F3",
							"#4685BC",
							"#B3ADDC",
							"#3D8AD7",
							"#81BBE3",
							"#919AD9",
							"#3AA6E9",
							"#8D9DC9",
							"#88ADE6")
row_side_color <- merge(MLST_types, MLST_types_only)

# Now that we have extracted the data we need from the CARD_matrix dataframe, clean it up so its ready to be imported into the heatmap
CARD_matrix <- CARD_matrix[, -(1:2)]
CARD_matrix <- as.matrix(CARD_matrix)

# Run heatmap using gplots heatmap.2
plot(c(0, 100), c(0, 100), type = "n", xlab = "", ylab = "")

CARD_heatmap <- heatmap.2(CARD_matrix, 
	col = rev(gray.colors(256, start = 0.5, end = 1)),
	RowSideColors = row_side_color$COLOR,
	tracecol = NA, 
	key = FALSE,
	lwid = c(1.5,4), 
	lhei = c(1.5,4),
	cexCol = 0.3,
	margins = c(10,15)
)

text(55, 0, "ABX Gene", cex = 0.75)
text(100, 50, "Isolate ID",cex = 0.75, srt = 90)

# Construct Legends and Text
legend_scale <- as.numeric(1.5)
text(0, 100, "MLST Type", col = "#7F7CA9", cex = 0.75, adj = c(0,1))
row_legend <- unique(subset(row_side_color, select = c("MLST", "COLOR")))
for (i in nrow(row_legend):1)
	{
	j = i * legend_scale
	rect(0, 98-j, 3, 98-j-legend_scale, border = NA, col = row_legend[i, 2])
	text(4, 98-j, row_legend[i, 1], col = row_legend[i, 2], cex = 0.50, adj = c(0,1))
	}