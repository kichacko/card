#/usr/utilities/card/bash

# Load the appropriate modules to run CARD
module load card/3.0.9
module load metagenemark

# All .faa files should be in their own directories. The directories should be labeled according to sample name
for i in *.faa
	do
	dir="${i%%.faa}"
	mkdir $dir
	mv $i ./$dir
done

# Run CARD, both rgi.py and then cleanup using convertJsonToTSV.py
for i in *
	do
	cd $i
	python $CARD_HOME/rgi.py -t protein -i *.faa -o “${i}_CARD”
	python $CARD_HOME/convertJsonToTSV.py -i *.json -o “${i}_CARD” 
	cd ..
done

# Still need to combine the final outputs into one file to run through heatmap.r

for i in *
do
cd $i
awk -v var="${i}" ' BEGIN{OFS=FS="\t"}
{if (NR != 1)
	{print var, $0}
	}' "${i}_CARD.txt" > "${i}_CARD_clean.txt"
cp "${i}_CARD_clean.txt" ../
cd ..
done

# Combine all the CARD files together and add header line
for i in *clean.txt; do cat $i >> combined_CARD_clean.txt; done
awk 'BEGIN{print "ISOLATE_ID" "\t" "ORF_ID" "\t" "CONTIG" "\t" "START" "\t" "STOP" "\t" "ORIENTATION" "\t" "CUT_OFF" "\t" "PASS_EVALUE" "\t" "Best_Hit_evalue" "\t" "Best_Hit_ARO" "\t" "Best_Identities" "\t" "ARO" "\t" "ARO_name" "\t" "Model_type" "\t" "SNP" "\t" "AR0_category" "\t" "bit_score"}1' combined_CARD_clean.txt > combined_CARD.txt
rm *clean.txt

