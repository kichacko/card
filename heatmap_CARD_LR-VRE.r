#!/usr/utilities/card/

# Load required libraries
library("reshape2")
library("gplots")

Combined_CARD <- read.delim("~/Google Drive/1 - Lab/1 - Antibiotic Resistance/1 - LR-VRE/4 - CARD/Combined_CARD.txt")
GENE_renaming <- read.delim("~/Google Drive/1 - Lab/1 - Antibiotic Resistance/1 - LR-VRE/4 - CARD/GENE_renaming.txt")
ISOLATE_ID <- read.delim("~/Google Drive/1 - Lab/1 - Antibiotic Resistance/1 - LR-VRE/4 - CARD/ISOLATE_ID.txt")

CARD_full <- Combined_CARD
CARD_full <- merge(CARD_full, GENE_renaming, all = TRUE)
CARD_full <- merge(CARD_full, ISOLATE_ID, all =  TRUE)

CARD_full$HEATMAP <- ifelse(CARD_full$CUT_OFF == "Strict", 1, ifelse(CARD_full$CUT_OFF == "Perfect", 1, 0))

CARD_perfect_strict <- subset(CARD_full, CARD_full$CUT_OFF == "Perfect" | CARD_full$CUT_OFF == "Strict")

CARD_dataframe <- subset(CARD_perfect_strict, select = c("ISOLATE", "GENE", "CLASS", "DRUG", "HEATMAP"))

CARD_matrix <- dcast(CARD_dataframe, CARD_dataframe$ISOLATE ~ CARD_dataframe$GENE, value.var="HEATMAP", fun.aggregate = length)
names(CARD_matrix)[1] <- "ISOLATE_ID"
rownames(CARD_matrix) <- CARD_matrix$ISOLATE_ID

DRUG_types <- subset(CARD_dataframe, select = c("GENE", "DRUG"))
DRUG_types <- unique(DRUG_types)
DRUG_types_only <- unique(subset(DRUG_types, select = c("DRUG")))
DRUG_types_only$COLOR <- c("#66b84b",
    "#6d43c6",
    "#b39d3a",
    "#cc54c8",
    "#55865d",
    "#d24c37",
    "#769ac6",
    "#c48661",
    "#655296",
    "#753633",
    "#c7598a")
    
col_side_color <- merge(DRUG_types, DRUG_types_only)
col_side_color <- col_side_color[order(col_side_color$GENE), ]

CARD_matrix <- CARD_matrix[, -1]
CARD_matrix <- as.matrix(CARD_matrix)

plot(c(0, 100), c(0, 100), type = "n", xlab = "", ylab = "")
CARD_heatmap <- heatmap.2(CARD_matrix,
    col = rev(gray.colors(256, start = 0, end = 1)),
    Rowv = NULL,
    dendrogram = ("column"),
    ColSideColors = col_side_color$COLOR,
    tracecol = NA,
    key = FALSE,
    margins = c(21,13))

text(55, 0, "ABX Gene", cex = 0.75)
text(100, 50, "Isolate ID",cex = 0.75, srt = 90)

legend_scale <- as.numeric(3)

text(0, 71, "Drug Name", cex = 0.75, adj = c(0,1))
col_legend <- unique(subset(col_side_color, select = c("DRUG", "COLOR")))
for (i in nrow(col_legend):1)
{
	j = i * legend_scale
	rect(0, 70-j, 8, 70-j-legend_scale, border = NA, col = col_legend[i, 2])
	text(9, 70-j, col_legend[i, 1], col = col_legend[i, 2], cex = 0.50, adj = c(0,1))
}


